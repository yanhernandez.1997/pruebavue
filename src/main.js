/*!

 =========================================================
 * Vue Paper Dashboard - v2.0.0
 =========================================================

 * Product Page: http://www.creative-tim.com/product/paper-dashboard
 * Copyright 2019 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard/blob/master/LICENSE.md)

 =========================================================

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 */
import Vue from "vue";
import App from "./App";
import router from "./router/index";

import PaperDashboard from "./plugins/paperDashboard";
import VueFormulate from '@braid/vue-formulate'
import "vue-notifyjs/themes/default.css";
import { auth } from "@/conexion";


Vue.use(VueFormulate);
Vue.use(PaperDashboard);

// GOOD
router.beforeEach( async (to, from, next) => {
  auth.onAuthStateChanged((u) => {
    if (to.name !== 'Login' && !u) next({ name: 'Login' })
    else next()
  });
});

router.beforeEach( async (to, from, next) => {
  auth.onAuthStateChanged((u) => {
    if (to.name === 'Login' && u) next({ name: 'Lista de productos' })
    else next()
  });
});

/* eslint-disable no-new */
new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
