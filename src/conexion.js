var firebase = require('firebase/app');
require('firebase/auth');
require('firebase/database');
require('firebase/storage');

var firebaseConfig = {
  apiKey: "AIzaSyDatcuxBz-1TNH5_I1fxIL43pJ-XH9Amsc",
  authDomain: "tienda-460ed.firebaseapp.com",
  databaseURL: "https://tienda-460ed.firebaseio.com",
  projectId: "tienda-460ed",
  storageBucket: "tienda-460ed.appspot.com",
  messagingSenderId: "359760076740",
  appId: "1:359760076740:web:8279fc020d487030d48e91",
  measurementId: "G-1PPXLPFXP2"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const db = firebase.database();
export const storage = firebase.storage();
export const auth = firebase.auth();